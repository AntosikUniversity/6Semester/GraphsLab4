const canvas = document.querySelector(".canvas");
const matrix = document.querySelector('.matrix');
const distances = document.querySelector('.tools__distances');

canvas.width = canvas.offsetWidth;
canvas.height = canvas.offsetHeight;

let graph = new Graph(canvas);
let moveVertex = false;
let movingVertex = undefined;


/* MATRIX */
matrix.placeholder = matrix.placeholder.replace(/\\n/g, '\n');

matrix.addEventListener('change', () => {
  const data = strToMatrix(matrix.value);
  const recalculatedMatrix = graph.build(data);
  matrix.value = recalculatedMatrix.map(line => line.join(' ')).join('\n');
});

/* CANVAS */
canvas.addEventListener('mousedown', function (e) {
  const mouseX = e.layerX;
  const mouseY = e.layerY;
  const vertexFound = graph.findCurrentVertex(mouseX, mouseY);

  if (vertexFound) {
    moveVertex = true;
    movingVertex = vertexFound;

    distances.innerHTML = "";

    graph.vertexes.forEach((vertex) => {
      if (vertex !== vertexFound) {
        const [path, length] = graph.AStar(vertexFound, vertex);

        const distanceDiv = document.createElement('div');
        distanceDiv.onmouseover = () => showPath(path);
        distanceDiv.onmouseout = () => graph.redraw();
        distanceDiv.classList.add('distance');
        distanceDiv.innerHTML = `${path.join(" → ")}: <b>${length}</b>`;
        distances.appendChild(distanceDiv);
      }
    })
  }
});

canvas.addEventListener('mousemove', function (e) {
  if (moveVertex) {
    const x = e.layerX;
    const y = e.layerY;

    graph.moveVertex(movingVertex, { x, y });
  }
});

canvas.addEventListener('mouseup', () => {
  moveVertex = false;
  movingVertex = false;
});

canvas.addEventListener('mouseleave', () => {
  moveVertex = false;
  movingVertex = false;
});

function strToMatrix(string) {
  const data = string.split('\n').map(line => line.replace(/\W+/ig, " ").replace(/\s+/ig, " ").trim().split(' ').map(el => Number(el) || 0));
  const size = Math.max(data.length, ...data.map(line => line.length));

  const result = [];
  for (let i = 0; i < size; ++i) {
    let columns = [];
    for (let j = 0; j < size; ++j) {
      columns[j] = (data[i] && data[i][j]) || 0;
    }
    result[i] = columns;
  }

  for (let i = 0; i < size; ++i) {
    for (let j = 0; j < size; ++j) {
      if (result[i][j] !== result[j][i]) {
        if (result[i][j] === 0 || result[j][i] === 0)
          result[i][j] = result[j][i] = Math.max(result[i][j], result[j][i]);
        else
          result[i][j] = result[j][i] = Math.min(result[i][j], result[j][i]);
      }
    }
  }

  return result;
}

function showPath(path) {
  const vertexIndexes = path.map(Number);

  const vertexesHightlight = graph.vertexes.filter(vertex => vertexIndexes.includes(vertex.index));
  const edgesHighlight = graph.edges.filter(edge => {
    const vertex1Index = vertexIndexes.indexOf(edge.vertex1.index);
    const vertex2Index = vertexIndexes.indexOf(edge.vertex2.index);

    if (vertex1Index < 0 || vertex2Index < 0) return false;
    else return vertex1Index === vertex2Index + 1 || vertex1Index === vertex2Index - 1;
  });

  graph.highlight(edgesHighlight, vertexesHightlight);
}

/* MODAL */
function showModal() {
  document.querySelector('.modal').classList.add('modal--show');
  document.querySelector('.overlay').classList.add('overlay--show');
}

function closeModal() {
  document.querySelector('.modal').classList.remove('modal--show');
  document.querySelector('.overlay').classList.remove('overlay--show');
}

document.querySelector('.tools__controls__matrix').addEventListener('click', () => showModal());
document.querySelector('.modal__close').addEventListener('click', () => closeModal());
document.querySelector('.overlay').addEventListener('click', () => closeModal())

/* HELPERS */
window.addEventListener('resize', () => {
  canvas.width = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
  graph.redraw();
});